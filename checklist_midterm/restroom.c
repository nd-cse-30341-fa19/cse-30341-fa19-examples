/* restroom.c: semaphore */

#include "thread.h"

#include <semaphore.h>
#include <unistd.h>

/* Type Definitions */

typedef enum {
    MALE,
    FEMALE,
} Gender;

/* Constants */

const size_t NTHREADS = 32;

/* Globals */

size_t Counts[2] = {0};
sem_t  CountsLock;
sem_t  GendersLock[2];
sem_t  BathroomQueue;

/* Macros */

#define OTHER(g)    ((g + 1) % 2)

/* Threads */

void restroom_enter(Gender g) {
    sem_wait(&BathroomQueue);
    sem_wait(&GendersLock[g]);
    sem_wait(&CountsLock);
    if (Counts[g] == 0) {
        sem_wait(&GendersLock[OTHER(g)]);   
    }
    Counts[g]++;
    sem_post(&BathroomQueue);
    sem_post(&GendersLock[g]);
    sem_post(&CountsLock);
}

void restroom_leave(Gender g) {
    sem_wait(&CountsLock);
    Counts[g]--;
    if (Counts[g] == 0) {
        sem_post(&GendersLock[OTHER(g)]);
    }
    sem_post(&CountsLock);
}

void * person(void *arg) {
    Gender g = (Gender)(arg);
    restroom_enter(g);
    sleep(1);
    printf("Males: %lu, Females: %lu\n", Counts[MALE], Counts[FEMALE]);
    restroom_leave(g);
    return 0;
}

/* Main execution */

int main(int argc, char *argv[]) {
    Thread t[NTHREADS];

    sem_init(&CountsLock         , 0, 1);
    sem_init(&GendersLock[MALE]  , 0, 1);
    sem_init(&GendersLock[FEMALE], 0, 1);
    sem_init(&BathroomQueue      , 0, 1);

    srand(time(NULL));

    for (size_t i = 0; i < NTHREADS; i++) {
    	thread_create(&t[i], NULL, person, (void *)(long)((rand() % 3) < 1));
    }

    for (size_t i = 0; i < NTHREADS; i++) {
    	thread_join(t[i], NULL);
    }

    return 0;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
